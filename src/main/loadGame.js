import { createGrid } from "./grid.js";
import { createGame } from "./memory.js";

document.addEventListener("DOMContentLoaded", () => {
    console.log("launching game...");
    const params = new URLSearchParams(window.location.search)
    let shuffle = params.get('no-shuffle') === null 
    const grid = createGrid({shuffle})
    const game = createGame(grid)
    for (let rowIdx = 0; rowIdx < grid.length; rowIdx++) {
        const row = grid[rowIdx];
        for (let colIdx = 0; colIdx < row.length; colIdx++) {
            const card = row[colIdx];
            const pageCard = document.querySelector('#row_' + rowIdx + '-col_' + colIdx)
            card.listener = function () {
                if (card.isVisible) {
                    pageCard.src = 'images/' + card.type + '.jpeg'
                }
                else {
                    pageCard.src = 'images/dos.jpeg'
                }
            }
            pageCard.addEventListener('click', function() {
                if (!card.isVisible) {
                    game.reveal(card)
                }
            })
        }
    }
})