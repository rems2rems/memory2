import timeout from "./util/timeout.js"

function createGame(grid) {
    return {
        card1: null,
        card2: null,
        reveal: async function (card) {
            card.setVisible(!card.isVisible)
            card.listener()
            
            if (this.card1 == null) {
                this.card1 = card
                return
            }
            this.card2 = card
            
            if(this.card1.type !== this.card2.type){
                await timeout(1000)
                
                this.card1.setVisible(false)
                this.card1.listener()
                this.card2.setVisible(false)
                this.card2.listener()
                
            }
            
            this.card1 = null
            this.card2 = null
        }
    }
}

export {createGame }