import { shuffleArray } from "./util/shuffleArray.js"

function createTypes(){
    let types = ['dog1','dog2','kangaroo','gorilla','lama','giraffe','squirrel','horse','lemurian','bear']
    types = types.concat(types)
    return types    
}
    
function createGrid({shuffle} = {}){
    const grid = []
    const types = createTypes()
    for (let rowIdx = 0; rowIdx < 5; rowIdx++) {
        const row = []
        for (let colIdx = 0; colIdx < 4; colIdx++) {
            row.push({ isVisible : false,
                type : types[0],
                setVisible : function(isVisible){
                    this.isVisible = isVisible
                }
            })
            types.splice(0,1)
        }
        grid.push(row)
    }
    if(shuffle){
        shuffleArray(grid)
    }
    return grid
}

export { createGrid, createTypes }