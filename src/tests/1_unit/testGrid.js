import { createGrid, createTypes } from '../../main/grid.js'
import chai from 'chai'
const expect = chai.expect

describe("a memory grid",()=>{

    it("must have correct dimension",()=>{

        const grid = createGrid({shuffle:false})
        expect(grid.length).to.equal(5)
        expect(grid[0].length).to.equal(4)
    })

    it("must have correct cells",()=>{

        const grid = createGrid({shuffle:false})
        const topLeft = grid[0][0]
        expect(topLeft).to.have.property('isVisible')
        expect(topLeft.isVisible).to.equal(false)
        expect(topLeft).to.have.property('type')
        expect(topLeft.type).to.be.oneOf(createTypes())
        expect(topLeft).to.have.property('setVisible')
        expect(topLeft.setVisible).to.be.an.instanceof(Function)
        topLeft.setVisible(true)
        expect(topLeft.isVisible).to.equal(true)
    })

    it("must be correctly shuffled",()=> {

        let globalScore = 0;
        for (let i = 0; i < 1000; i++) {
            const grid1 = createGrid({shuffle:true})
            const grid2 = createGrid({shuffle:true})
            let nbIdenticalLocations = 0
            for (const [rowIdx,row] of grid1.entries()) {
                for (const [colIdx] of row.entries()) {
                    if(grid1[rowIdx][colIdx].type === grid2[rowIdx][colIdx].type){
                        nbIdenticalLocations += 1
                    }
                }    
            }
            const similarityScore = nbIdenticalLocations/(grid1.length*grid1[0].length)
            globalScore += similarityScore
            // if(similarityScore >= 0.5){
            //     console.log("nb",nbIdenticalLocations,"ressemble",similarityScore,"1",grid1[0][0].type,"2",grid2[0][0].type);
            // }
        }
        globalScore /= 1000.0;
        // console.log(globalScore);
        expect(globalScore).to.be.below(0.25)
    })
})